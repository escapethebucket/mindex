﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using challenge.Services;
using challenge.Models;

namespace challenge.Controllers
{
    [Route("api/compensation")]
    public class CompensationController : Controller
    {
        private readonly ILogger _logger;
        private readonly ICompensationService _compensationService;

        public CompensationController(ILogger<CompensationController> logger, ICompensationService compensationService)
        {
            _logger = logger;
            _compensationService = compensationService;
        }

        /*
         * Task #2 - Get Compensation for employee.
         */
        [HttpGet("{id}", Name = "getCompensationById")]
        public IActionResult GetCompensationById(string id)
        {
            _logger.LogDebug($"Received compensation get request for '{id}'");

            Compensation retVal = _compensationService.GetById(id);

            if (retVal == null)
                return NotFound();

            return Ok(retVal);
        }

        /*
         * Task #2 - Create Compensation for new employee or edit compensation for previous employees.
         */
        [HttpPut("{id}")]
        public IActionResult UpdateCompensation(String id, [FromBody]Compensation newCompensation)
        {
            _logger.LogDebug($"Received compensation update request for '{id}'");

            Compensation existingCompensation = _compensationService.GetById(id);
            if (existingCompensation == null)
            {
                _compensationService.Create(newCompensation);
            }
            else
            {
                _compensationService.Replace(existingCompensation, newCompensation);
            }

            return Ok(newCompensation);
        }
    }
}
