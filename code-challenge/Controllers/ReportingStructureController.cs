﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using challenge.Services;
using challenge.Models;

namespace challenge.Controllers
{
    [Route("api/reportingstructure")]
    public class ReportingStructureController : Controller
    {
        private readonly ILogger _logger;
        private readonly IEmployeeService _employeeService;

        /*
         * Constructor for Task 1 Controller.
         * There's no need to create an entirely new Employee service at for this task.
         */
        public ReportingStructureController(ILogger<ReportingStructureController> logger, IEmployeeService employeeService)
        {
            _logger = logger;
            _employeeService = employeeService;
        }

        /*
         * Task #1: Get the Report Structure of an Employee based on ID.  Return the Employee object and the numberOfReports.
         */
        [HttpGet("{id}", Name = "getReportStructureById")]
        public IActionResult GetReportStructureById(String id)
        {
            _logger.LogDebug($"Received reporting structure get request for '{id}'");

            Employee employee = _employeeService.GetById(id);

            if (employee == null)
                return NotFound();

            ReportingStructure retVal = new ReportingStructure(employee);

            return Ok(retVal);
        }
    }
}
