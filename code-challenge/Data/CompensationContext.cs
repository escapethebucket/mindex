﻿using challenge.Models;
using Microsoft.EntityFrameworkCore;

namespace challenge.Data
{
    /*
     * Task #2 - Context Created for new Compensation Database.
     */
    public class CompensationContext : DbContext
    {
        public CompensationContext(DbContextOptions<CompensationContext> options) : base(options)
        {

        }

        public DbSet<Compensation> Compensations { get; set; }
    }
}