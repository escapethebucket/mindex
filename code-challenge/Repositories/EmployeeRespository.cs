﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using challenge.Models;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using challenge.Data;

namespace challenge.Repositories
{
    public class EmployeeRespository : IEmployeeRepository
    {
        private readonly EmployeeContext _employeeContext;
        private readonly ILogger<IEmployeeRepository> _logger;

        public EmployeeRespository(ILogger<IEmployeeRepository> logger, EmployeeContext employeeContext)
        {
            _employeeContext = employeeContext;
            _logger = logger;
        }

        public Employee Add(Employee employee)
        {
            employee.EmployeeId = Guid.NewGuid().ToString();
            _employeeContext.Employees.Add(employee);
            return employee;
        }

        public Employee GetById(string id)
        {
            /* 
             * CODE NOTE - There is some type of race condition with the original call, setting the DirectReports to null.
             * It appears to work when I put a breakpoint there for several seconds, but not otherwise.
             * Creating the DBSet as a List and then parsing the list seems to fix the issue.
             * return _employeeContext.Employees.SingleOrDefault(e => e.EmployeeId == id);
             */
            Employee retVal = null;
            DbSet<Employee> eSet = _employeeContext.Employees;
            List<Employee> eList = eSet.ToList();
            retVal = eList.SingleOrDefault(e => e.EmployeeId == id);
            return retVal;
        }

        public Task SaveAsync()
        {
            return _employeeContext.SaveChangesAsync();
        }

        public Employee Remove(Employee employee)
        {
            return _employeeContext.Remove(employee).Entity;
        }
    }
}
