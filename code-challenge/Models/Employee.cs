﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace challenge.Models
{
    public class Employee
    {
        public String EmployeeId { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Position { get; set; }
        public String Department { get; set; }
        public List<Employee> DirectReports { get; set; }

        /*
         * Default Constructor
         * Note: DirectorReports set to an empty list instead of null for Task #1.
         */
        public Employee()
        {
            this.DirectReports = new List<Employee>();
        }

        /*
         * Copy Constructor
         * Note: Task #2.
         */
        public Employee(Employee e)
        {
            this.Department = e.Department;
            this.DirectReports = e.DirectReports;
            this.EmployeeId = e.EmployeeId;
            this.FirstName = e.FirstName;
            this.LastName = e.LastName;
            this.Position = e.Position;
        }
    }
}
