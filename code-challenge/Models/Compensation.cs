﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace challenge.Models
{
    /*
     *  Task #2 - Class created for new Compensation Model.
     */
    public class Compensation
    {
        [Key]
        public string EmployeeId { get; set; }

        public decimal Salary { get; set; }

        public DateTime EffectiveDate { get; set; }

        /*
         * Default Constructor
         */
        public Compensation()
        {
            this.EmployeeId = string.Empty;
            this.Salary = 0.0M;
            this.EffectiveDate = DateTime.Today;
        }
    }
}
