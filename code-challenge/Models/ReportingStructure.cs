﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace challenge.Models
{
    public class ReportingStructure
    {
        public Employee employee { get; set; }

        // Task #1: Recursion Test
        public int numberOfReports
        {
            get
            {
                int retVal = 0;
                foreach (Employee e in this.employee.DirectReports.ToList())
                {
                    retVal++;
                    ReportingStructure structure = new ReportingStructure(e);
                    retVal += structure.numberOfReports;
                }
                return retVal;
            }
        }

        /*
         * Default Constructor
         */
        public ReportingStructure() { }

        /*
         * Constructor based on Employee parameter.
         */
        public ReportingStructure(Employee e)
        {
            this.employee = e;
        }
    }
}
