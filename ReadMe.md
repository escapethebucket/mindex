# Mindex Coding Challenge

## Task #1
Created a new type, ReportingStructure.
Performed recursion test for numberOfReports in ReportingStructure.cs
* READ
    * HTTP Method: GET 
    * URL: localhost:8080/api/reportingstructure/{id}
    * RESPONSE: Reporting Structure

## Task #2
Created a new Compensation database and seeding.
Allowed for modifying previous employee compensation and adding new employee compensation.
Compensation not automatically created when new employee created (outside scope of project).
* READ
    * HTTP Method: GET 
    * URL: localhost:8080/api/compensation/{id}
    * RESPONSE: Compensation
* UPDATE
    * HTTP Method: PUT 
    * URL: localhost:8080/api/compensation/{id}
    * PAYLOAD: Compensation
    * RESPONSE: Compensation